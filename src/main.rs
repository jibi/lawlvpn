// Copyright (C) 2017 Gilberto Bertin <me@jibi.io>
//
// This file is part of lawlvpn.
//
// lawlvpn is free software: you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation, either version 3 of the License, or
// (at your option) any later version.
//
// lawlvpn is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.
//
// You should have received a copy of the GNU General Public License
// along with lawlvpn.  If not, see <http://www.gnu.org/licenses/>.

#![feature(untagged_unions)]

extern crate openssl;
extern crate log;
extern crate rustc_serialize;
extern crate clap;
extern crate core;

mod tap;
mod peer;
mod epoll;
mod epoll_engine;
mod config;
mod hex;

use std::os::unix::io::{RawFd, AsRawFd};
use std::collections::HashMap;
use std::net::{TcpListener, TcpStream};
use std::env;

use clap::{Arg, App};

use config::Config;

fn parse_args<'a, 'b>() -> clap::App<'a, 'b> {
    App::new("lawlvpn")
        .version(env!("CARGO_PKG_VERSION"))
        .author("jibi <me@jibi.io>")
        .arg(Arg::with_name("config")
             .short("c")
             .long("config")
             .help("The path to the configuration file.")
             .takes_value(true))
}

fn load_config(config_file: Option<&str>) -> Config {
    let config_file = match config_file {
        Some(x) => String::from(x),
        None => {
            let mut path = env::home_dir().unwrap();
            path.push(".config/lawlvpn/config.toml");

            String::from(path.to_str().unwrap())
        }
    };

    Config::load(&config_file).unwrap()
}

fn init_taps(c: &Config) -> HashMap<RawFd, tap::Tap> {
    println!("[!] Setup {} interface", c.tap().name());

    let mut taps = HashMap::new();
    let name = c.tap().name();
    let addr = c.tap().addr();
    let tap = Box::new(tap::Tap::new(name, addr)).unwrap();
    let fd = tap.file.as_raw_fd();

    taps.insert(fd, tap);

    taps
}

fn init_listener(c: &Config) -> TcpListener {
    println!("[!] Listening on {}", c.bind());

    TcpListener::bind(c.bind()).unwrap()
}

fn connect_to_peers(c: &Config) -> HashMap<RawFd, peer::Peer> {
    let mut peers = HashMap::new();

    if c.peers().is_none() {
        return peers;
    }

    println!("[!] Connecting to peers");
    let peer_addrs = c.peers().unwrap();

    for p in peer_addrs {
        let stream = TcpStream::connect(p.addr()).unwrap();
        let peer = peer::Peer::new(stream);
        let fd = peer.stream.as_raw_fd();

        peers.insert(fd, peer);
    }

    peers
}

fn main() {
    let args = parse_args().get_matches();
    let config = load_config(args.value_of("config"));

    println!("[!] Welcome {} to lawlvpn {}", config.user_name(), env!("CARGO_PKG_VERSION"));

    let taps = init_taps(&config);
    let listener = init_listener(&config);
    let peers = connect_to_peers(&config);

    let mut epoll_engine = epoll_engine::EpollEngine::new(taps, listener, peers);

    epoll_engine.main_loop();
}
