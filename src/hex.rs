// Copyright (C) 2017 Gilberto Bertin <me@jibi.io>
//
// This file is part of lawlvpn.
//
// lawlvpn is free software: you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation, either version 3 of the License, or
// (at your option) any later version.
//
// lawlvpn is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.
//
// You should have received a copy of the GNU General Public License
// along with lawlvpn.  If not, see <http://www.gnu.org/licenses/>.

#![allow(dead_code)]

pub fn dump(array: &Vec<u8>) {
    for i in 0 .. array.len() {
        print!("{:02X} ", array[i]);

        if i % 16 == 15 || i == array.len() - 1 {
            for _ in 0 .. 15 - (i % 16) {
                print!("   ");
            }
            print!("| ");

            for j in (i - (i%16)) .. i {
                if array[j] > 15 && array[j] < 127 {
                    print!("{}", array[j] as char);
                } else {
                    print!(".");
                }
            }
            println!("");
        }
    }
    println!("");
}
