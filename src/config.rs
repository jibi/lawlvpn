// Copyright (C) 2017 Gilberto Bertin <me@jibi.io>
//
// This file is part of lawlvpn.
//
// lawlvpn is free software: you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation, either version 3 of the License, or
// (at your option) any later version.
//
// lawlvpn is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.
//
// You should have received a copy of the GNU General Public License
// along with lawlvpn.  If not, see <http://www.gnu.org/licenses/>.

extern crate toml;

use std::io::prelude::*;
use std::fs::File;

#[derive(Clone, RustcDecodable)]
pub struct Config {
    user_name: Option<String>,
    bind: Option<String>,
    tap: Option<TapConfig>,
    peers: Option<Vec<PeerConfig>>,
}

#[derive(Clone, RustcDecodable)]
pub struct TapConfig {
    name: Option<String>,
    addr: Option<String>,
}

#[derive(Clone, RustcDecodable)]
pub struct PeerConfig {
    addr: Option<String>,
}

impl Config {
    pub fn load(config_file: &str) -> Result<Self, &'static str> {
        let mut f = match File::open(config_file) {
            Ok(f) => f,
            Err(_) => return Err("Cannot open config file"),
        };
        let mut s = String::new();

        match f.read_to_string(&mut s) {
            Ok(_) => (),
            Err(_) => return Err("Cannot read config file"),
        };

        let decoded: Option<Config> = toml::decode_str(s.as_str());

        let decoded = {
            match decoded {
                None => return Err("Cannot decode config"),
                Some(d) => d,
            }
        };

        if decoded.user_name.is_none() {
            return Err("No tap interface in config");
        }

        if decoded.bind.is_none() {
            return Err("No tap interface in config");
        }


        if decoded.tap.is_none() {
            return Err("No user name specified in config");
        }

        Ok(decoded)
    }

    pub fn user_name(&self) -> &str {
        self.user_name.as_ref().map(AsRef::as_ref).unwrap()
    }

    pub fn bind(&self) -> &str {
        self.bind.as_ref().map(AsRef::as_ref).unwrap()
    }

    pub fn tap(&self) -> &TapConfig {
        self.tap.as_ref().unwrap()
    }

    pub fn peers(&self) -> Option<&Vec<PeerConfig>> {
        self.peers.as_ref()
    }
}

impl TapConfig {
    pub fn name(&self) -> &str {
        self.name.as_ref().map(AsRef::as_ref).unwrap()
    }

    pub fn addr(&self) -> &str {
        self.addr.as_ref().map(AsRef::as_ref).unwrap()
    }
}

impl PeerConfig {
    pub fn addr(&self) -> &str {
        self.addr.as_ref().map(AsRef::as_ref).unwrap()
    }
}
