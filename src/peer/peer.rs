// Copyright (C) 2017 Gilberto Bertin <me@jibi.io>
//
// This file is part of lawlvpn.
//
// lawlvpn is free software: you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation, either version 3 of the License, or
// (at your option) any later version.
//
// lawlvpn is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.
//
// You should have received a copy of the GNU General Public License
// along with lawlvpn.  If not, see <http://www.gnu.org/licenses/>.

use std::net::TcpStream;
use std::os::unix::io::AsRawFd;
use epoll;

pub struct Peer {
    pub stream: TcpStream,
    pub epoll_data: *mut epoll::epoll_event_data,
}

impl Peer {
    pub fn new(stream: TcpStream) -> Self {
        let fd = stream.as_raw_fd();
        let epoll_data = Box::into_raw(Box::new(epoll::epoll_event_data{fd_type: epoll::FdType::Peer, fd: fd}));

        Peer {
            stream: stream,
            epoll_data: epoll_data,
        }
    }
}

