// Copyright (C) 2017 Gilberto Bertin <me@jibi.io>
//
// This file is part of lawlvpn.
//
// lawlvpn is free software: you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation, either version 3 of the License, or
// (at your option) any later version.
//
// lawlvpn is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.
//
// You should have received a copy of the GNU General Public License
// along with lawlvpn.  If not, see <http://www.gnu.org/licenses/>.

use tap::tap_dev;
use std::fs::File;

pub struct Tap {
    pub iface: String,
    pub addr:  String,
    pub file:  File,
}

impl Tap {
    pub fn new(iface: &str, addr: &str) -> Option<Self> {
        let file = tap_dev::alloc_tun(iface).unwrap();

        tap_dev::set_tun_addr(iface, addr);

        let tap = Tap {
            iface: String::from(iface),
            addr: String::from(addr),
            file: file,
        };

        Some(tap)
    }
}

