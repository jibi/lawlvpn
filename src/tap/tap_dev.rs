// Copyright (C) 2017 Gilberto Bertin <me@jibi.io>
//
// This file is part of lawlvpn.
//
// lawlvpn is free software: you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation, either version 3 of the License, or
// (at your option) any later version.
//
// lawlvpn is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.
//
// You should have received a copy of the GNU General Public License
// along with lawlvpn.  If not, see <http://www.gnu.org/licenses/>.

extern crate libc;

use self::libc::ioctl;

use std::fs::File;
use std::fs::OpenOptions;
use std::os::unix::io::AsRawFd;
use std::process::Command;

mod tap_dev_ffi {
    #![allow(dead_code,non_snake_case,non_upper_case_globals,improper_ctypes,non_camel_case_types)]
    include!(concat!(env!("OUT_DIR"), "/tap.rs"));

    // Bindgen is not smart enough to get TUNSETIF and the if_req
    // struct right, so let's manually import them
    pub const TUNSETIFF: u64 = 0x400454ca;

    pub struct TunIoctl {
        pub ifr_name:  [u8; IFNAMSIZ as usize],
        pub ifr_flags: i16,
    }
}

pub fn alloc_tun(name: &str) -> Option<File> {
        let file: File = match OpenOptions::new().read(true).write(true).open("/dev/net/tun") {
            Ok(file) => file,
            Err(_) => return None,
        };

        let mut ifr = tap_dev_ffi::TunIoctl{
            ifr_name: [0; tap_dev_ffi::IFNAMSIZ as usize],
            ifr_flags: (tap_dev_ffi::IFF_TAP | tap_dev_ffi::IFF_NO_PI) as i16
        };

        for (i, c) in name.chars().take(tap_dev_ffi::IFNAMSIZ as usize).enumerate() {
            ifr.ifr_name[i] = c as u8;
        }

        let _ = unsafe {
            let ret = ioctl(file.as_raw_fd(), tap_dev_ffi::TUNSETIFF, &ifr);
            if ret != 0 {
                return None
            }
        };

        Some(file)
}

pub fn set_tun_addr(iface: &str, addr: &str) {
        Command::new("ip")
            .args(&["addr", "add", addr, "dev", iface])
            .output()
            .expect("failed to execute 'ip addr' process");

        Command::new("ip")
            .args(&["link", "set", "dev", iface, "up"])
            .output()
            .expect("failed to execute 'ip link' process");
}
