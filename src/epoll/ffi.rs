// Copyright (C) 2017 Gilberto Bertin <me@jibi.io>
//
// This file is part of lawlvpn.
//
// lawlvpn is free software: you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation, either version 3 of the License, or
// (at your option) any later version.
//
// lawlvpn is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.
//
// You should have received a copy of the GNU General Public License
// along with lawlvpn.  If not, see <http://www.gnu.org/licenses/>.

#![allow(dead_code)]

use std::ptr;

pub const EPOLL_CTL_ADD: u32 = 1;
pub const EPOLL_CTL_DEL: u32 = 2;
pub const EPOLL_CTL_MOD: u32 = 3;

pub const EPOLLIN:  u32 = 1 << 0;
pub const EPOLLPRI: u32 = 1 << 1;
pub const EPOLLOUT: u32 = 1 << 2;
pub const EPOLLERR: u32 = 1 << 3;

#[repr(C)]
#[derive(Copy, Clone)]
pub enum FdType {
    Listener,
    Peer,
    Tap
}

#[repr(C, packed)]
pub struct epoll_event_data {
    pub fd:      i32,
    pub fd_type: FdType
}

#[repr(C, packed)]
#[derive(Copy, Clone)]
pub struct epoll_event {
    pub events: u32,
    pub data:   *mut epoll_event_data,
}

mod __libc_epoll {
    use epoll::epoll_event;

    extern {
        pub fn epoll_create1(flags: u32) -> i32;
        pub fn epoll_ctl(epfd: i32, op: u32, fd: i32, event: *const epoll_event) -> i32;
        pub fn epoll_wait(epfd: i32, events: *mut epoll_event, maxevents: i32, timeout: i32) -> i32;
    }
}

pub fn epoll_create1(flags: u32) -> i32 {
    unsafe {
        __libc_epoll::epoll_create1(flags)
    }
}

pub fn epoll_ctl(epfd: i32, op: u32, fd: i32, event: Option<&epoll_event>) -> i32 {
    let event = match event {
        Some(event) => event,
        None => ptr::null(),
    };
    unsafe {
        __libc_epoll::epoll_ctl(epfd, op, fd, event)
    }
}

pub fn epoll_wait(epfd: i32, events: &mut [epoll_event], timeout: i32) -> i32 {
    unsafe {
        __libc_epoll::epoll_wait(epfd, events.as_mut_ptr(), events.len() as i32, timeout)
    }
}

