// Copyright (C) 2017 Gilberto Bertin <me@jibi.io>
//
// This file is part of lawlvpn.
//
// lawlvpn is free software: you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation, either version 3 of the License, or
// (at your option) any later version.
//
// lawlvpn is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.
//
// You should have received a copy of the GNU General Public License
// along with lawlvpn.  If not, see <http://www.gnu.org/licenses/>.

use std::ptr;
use std::cell::UnsafeCell;

use std::net::{TcpListener};
use std::os::unix::io::{RawFd, AsRawFd};
use std::collections::HashMap;
use std::io::Read;
use std::io::Write;

use tap;
use peer;
use epoll;

pub struct EpollEngine {
    pub epoll_fd: i32,
    pub taps: HashMap<RawFd, tap::Tap>,
    pub listener: TcpListener,
    pub peers: HashMap<RawFd, peer::Peer>,
}

impl EpollEngine {
    fn add_fd(&self, fd: i32, epoll_data: *mut epoll::epoll_event_data) {
        let epoll_desc = &epoll::epoll_event { events: epoll::EPOLLIN, data: epoll_data };

        epoll::epoll_ctl(self.epoll_fd, epoll::EPOLL_CTL_ADD, fd, Some(epoll_desc));
    }

    fn del_fd(&self, fd: i32, epoll_data: *mut epoll::epoll_event_data) {
        epoll::epoll_ctl(self.epoll_fd, epoll::EPOLL_CTL_DEL, fd, None);
        unsafe {
            drop(Box::from_raw(epoll_data));
        }
    }

    pub fn add_tap_fd(&self, tap: &tap::Tap) {
        let fd = tap.file.as_raw_fd();
        let epoll_data = Box::into_raw(Box::new(epoll::epoll_event_data{fd_type: epoll::FdType::Tap, fd: fd}));
        self.add_fd(fd, epoll_data);
    }

    pub fn add_listener_fd(&self, listener: &TcpListener) {
        let fd = listener.as_raw_fd();
        let epoll_data = Box::into_raw(Box::new(epoll::epoll_event_data{fd_type: epoll::FdType::Listener, fd: fd}));
        self.add_fd(fd, epoll_data);
    }

    pub fn add_peer_fd(&self, peer: &peer::Peer) {
        let fd = peer.stream.as_raw_fd();
        let epoll_data = peer.epoll_data;
        self.add_fd(fd, epoll_data);
    }

    pub fn add_peer(&mut self, peer: peer::Peer) {
        let fd = peer.stream.as_raw_fd();
        self.add_peer_fd(&peer);
        self.peers.insert(fd, peer);
    }

    pub fn del_peer(&mut self, fd: i32, ev: &epoll::epoll_event) {
        self.peers.remove(&fd);
        self.del_fd(fd, ev.data);
    }

    pub fn new(taps: HashMap<RawFd, tap::Tap>, listener: TcpListener, peers: HashMap<RawFd, peer::Peer>) -> Self {
        let mut ee = EpollEngine {
            epoll_fd: 0,
            taps: taps,
            listener: listener,
            peers: peers,
        };

        ee.epoll_fd = epoll::epoll_create1(0);

        let ee = unsafe {
            let ee_cell = UnsafeCell::new(ee);
            let ref ee = *ee_cell.get();

            for tap in ee.taps.values() {
                ee.add_tap_fd(&tap);
            }

            ee.add_listener_fd(&ee.listener);

            for peer in ee.peers.values() {
                ee.add_peer_fd(&peer);
            }

            ee_cell.into_inner()
        };

        return ee;
    }

    fn handle_new_client(&mut self) {
        let s = self.listener.accept().unwrap();
        let peer = peer::Peer::new(s.0);

        self.add_peer(peer);
    }

    fn handle_tap(&mut self, ev: &epoll::epoll_event) {
        let mut buffer = [0; 2048];
        let fd = unsafe { (*ev.data).fd };
        let x = self.taps.get_mut(&fd).unwrap();

        let n = x.file.read(&mut buffer).unwrap();

        for peer in self.peers.values_mut() {
            peer.stream.write(&buffer[0..n]).unwrap_or(0);
        }
    }

    fn handle_peer(&mut self, ev: &epoll::epoll_event) {
        let mut buffer = [0; 2048];
        let fd = unsafe { (*ev.data).fd };

        let n = {
            let x = self.peers.get_mut(&fd).unwrap();
            x.stream.read(&mut buffer).unwrap()
        };

        if n <= 0 {
            self.del_peer(fd, ev);
            return;
        }

        for tap in self.taps.values_mut() {
            tap.file.write(&buffer[0..n]).unwrap();
        }
    }

    fn handle_ev(&mut self, ev: &epoll::epoll_event) {
        let fd_type = unsafe {
            (*ev.data).fd_type
        };

        match fd_type {
            epoll::FdType::Listener => {
                self.handle_new_client();
            },
            epoll::FdType::Tap => {
                self.handle_tap(ev);
            },
            epoll::FdType::Peer => {
                self.handle_peer(ev);
            }
        }
    }

    pub fn main_loop(&mut self) {
        loop {
            let mut evs = [epoll::epoll_event { events: 0, data: ptr::null_mut() }; 4];
            let ev_n = epoll::epoll_wait(self.epoll_fd, &mut evs, -1);

            for i in 0..ev_n {
                self.handle_ev(&evs[i as usize]);
            }
        }
    }
}
